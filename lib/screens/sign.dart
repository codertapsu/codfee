import 'dart:math';

import 'package:codfee/enums/text-field-direction.enum.dart';
import 'package:codfee/shared/widgets/custom-password-form-field.widget.dart';
import 'package:codfee/shared/widgets/custom-text-form-field.widget.dart';
import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:codfee/configs/label-text.const.dart';

import 'package:codfee/enums/auth-mode.enum.dart';

import 'package:codfee/ultils/utils.dart';

import 'package:codfee/shared/widgets/expandable.widget.dart';

class SignScreen extends StatefulWidget {
  SignScreen({Key key}) : super(key: key);

  @override
  _SignScreenState createState() => _SignScreenState();
}

class _SignScreenState extends State<SignScreen> with TickerProviderStateMixin {
  static const loadingDuration = const Duration(milliseconds: 400);

  final _signFormKey = GlobalKey<FormState>();

  final _passwordFocusNode = FocusNode();
  final _confirmPasswordFocusNode = FocusNode();

  final _passwordController = TextEditingController();

  AuthMode _authMode = AuthMode.SignIn;

  AnimationController _loadingController;
  AnimationController _switchAuthController;
  AnimationController _forgotPasswordController;
  AnimationController _postSwitchAuthController;

  Interval _nameTextFieldLoadingAnimationInterval;
  Interval _passTextFieldLoadingAnimationInterval;

  bool _isForgotPassword = false;

  AuthMode switchAuth(AuthMode mode) {
    if (mode == AuthMode.SignIn) {
      mode = AuthMode.SignUp;
    } else if (mode == AuthMode.SignUp) {
      mode = AuthMode.SignIn;
    }
    return mode;
  }

  void _onSwitchRecoveryPassword() {}

  void _onFormChanged() {}

  void _switchAuthMode() {
    _authMode = switchAuth(_authMode);
    if (_authMode == AuthMode.SignUp) {
      _switchAuthController.forward();
    } else {
      _switchAuthController.reverse();
    }
    setState(() {});
  }

  void _switchForgotPasswordMode() {
    _isForgotPassword = !_isForgotPassword;
    if (_isForgotPassword) {
      _forgotPasswordController.forward();
    } else {
      _forgotPasswordController.reverse();
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    _loadingController = AnimationController(
      vsync: this,
      duration: loadingDuration,
    )..addStatusListener((status) {
        // if (status == AnimationStatus.forward) {
        //   _logoController.forward();
        //   _titleController.forward();
        // }
        // if (status == AnimationStatus.reverse) {
        //   _logoController.reverse();
        //   _titleController.reverse();
        // }
      });

    _switchAuthController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );
    _forgotPasswordController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );
    _postSwitchAuthController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 150),
    );

    Future.delayed(const Duration(seconds: 1), () {
      _loadingController.forward();
    });

    _nameTextFieldLoadingAnimationInterval = const Interval(0, .85);
    _passTextFieldLoadingAnimationInterval = const Interval(.15, 1.0);
  }

  @override
  void dispose() {
    super.dispose();

    _loadingController.dispose();
    _switchAuthController.dispose();
    _forgotPasswordController.dispose();
    _postSwitchAuthController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final deviceSize = MediaQuery.of(context).size;
    final isSignIn = _authMode == AuthMode.SignIn;
    final cardWidth = min(deviceSize.width * 0.75, 360.0);
    const cardPadding = 16.0;
    final textFieldWidth = cardWidth - cardPadding * 2;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: deviceSize.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                transformHexCodeToColor('#667eea'),
                transformHexCodeToColor('#764ba2'),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 50),
                _buildLogo(),
                Card(
                  elevation: 1.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 20.0,
                      bottom: 20.0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _buildFormContainer(
                          theme,
                          isSignIn,
                          _isForgotPassword,
                          textFieldWidth,
                        ),
                        SizedBox(height: 5.0),
                        _switchForgotPasswordFormButton(theme),
                        SizedBox(height: 5.0),
                        _submitButton(theme, isSignIn),
                        SizedBox(height: 5.0),
                        _switchSignFormButton(theme, isSignIn),
                        SizedBox(height: 5.0),
                        _socialSignContainer(theme),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLogo() {
    return FlutterLogo(size: 150);
  }

  Widget _buildFormContainer(
    ThemeData theme,
    bool isSignIn,
    bool isForgotPassword,
    double width,
  ) {
    return Padding(
      padding: EdgeInsets.all(0),
      child: Form(
        key: _signFormKey,
        onChanged: _onFormChanged,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              child: _buildEmailField(width),
            ),
            ExpandableContainer(
              width: double.infinity,
              backgroundColor: Colors.transparent,
              controller: _forgotPasswordController,
              initialState: isForgotPassword
                  ? ExpandableContainerState.shrunk
                  : ExpandableContainerState.expanded,
              alignment: Alignment.topLeft,
              color: Colors.transparent,
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
                top: 20,
              ),
              onExpandCompleted: () => _postSwitchAuthController.forward(),
              child: Column(
                children: <Widget>[
                  _buildPasswordField(isSignIn),
                  ExpandableContainer(
                    width: double.infinity,
                    backgroundColor: Colors.transparent,
                    controller: _switchAuthController,
                    initialState: isSignIn
                        ? ExpandableContainerState.shrunk
                        : ExpandableContainerState.expanded,
                    alignment: Alignment.topLeft,
                    color: Colors.transparent,
                    padding: EdgeInsets.only(
                      top: 20,
                    ),
                    onExpandCompleted: () =>
                        _postSwitchAuthController.forward(),
                    child: _buildConfirmPasswordField(!isSignIn),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildEmailField(double width) {
    return CustomTextFormField(
      width: width,
      loadingController: _loadingController,
      interval: _nameTextFieldLoadingAnimationInterval,
      labelText: LabelText.defaultUsernameHint,
      prefixIcon: Icon(FontAwesomeIcons.solidUserCircle),
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) {
        FocusScope.of(context).requestFocus(_passwordFocusNode);
      },
      onSaved: (value) {},
    );
  }

  Widget _buildPasswordField(bool isSignIn) {
    return CustomPasswordFormField(
      animatedWidth: double.infinity,
      loadingController: _loadingController,
      interval: _passTextFieldLoadingAnimationInterval,
      labelText: LabelText.defaultPasswordHint,
      controller: _passwordController,
      textInputAction: isSignIn ? TextInputAction.done : TextInputAction.next,
      focusNode: _passwordFocusNode,
      onFieldSubmitted: (value) {
        // if (auth.isLogin) {
        //   _submit();
        // } else {
        //   // SignUp
        //   FocusScope.of(context)
        //       .requestFocus(_confirmPasswordFocusNode);
        // }
      },
      onSaved: (value) {},
    );
  }

  Widget _buildConfirmPasswordField(bool isSignUp) {
    // return CustomPasswordFormField(
    return CustomPasswordFormField(
      enabled: isSignUp,
      animatedWidth: double.infinity,
      loadingController: _loadingController,
      interval: _passTextFieldLoadingAnimationInterval,
      labelText: LabelText.defaultConfirmPasswordHint,
      textInputAction: TextInputAction.done,
      focusNode: _confirmPasswordFocusNode,
      onFieldSubmitted: (value) {
        // if (auth.isLogin) {
        //   _submit();
        // } else {
        //   // SignUp
        //   FocusScope.of(context)
        //       .requestFocus(_confirmPasswordFocusNode);
        // }
      },
      onSaved: (value) {},
    );
    //   animatedWidth: width,
    //   enabled: isSignUp,
    //   loadingController: _loadingController,
    //   inertiaController: _postSwitchAuthController,
    //   inertiaDirection: TextFieldInertiaDirection.right,
    //   labelText: LabelText.defaultConfirmPasswordHint,
    //   textInputAction: TextInputAction.done,
    //   focusNode: _confirmPasswordFocusNode,
    //   onFieldSubmitted: (value) {
    //     // _submit()
    //   },
    //   validator: isSignUp
    //       ? (value) {
    //           if (value != _passwordController.text) {
    //             return 'messages.confirmPasswordError';
    //           }
    //           return null;
    //         }
    //       : (value) => null,
    // );
  }

  Widget _switchForgotPasswordFormButton(ThemeData theme) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Text(LabelText.defaultForgotPasswordButton),
      disabledTextColor: theme.primaryColor,
      onPressed: _switchForgotPasswordMode,
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      textColor: theme.primaryColor,
    );
  }

  Widget _submitButton(ThemeData theme, bool isSignIn) {
    return MaterialButton(
      height: 40.0,
      minWidth: 120.0,
      color: theme.primaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Text(
        isSignIn
            ? LabelText.defaultLoginButton.toUpperCase()
            : LabelText.defaultSignupButton,
        textAlign: TextAlign.left,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      onPressed: _onSwitchRecoveryPassword,
      splashColor: Colors.redAccent,
    );
  }

  Widget _switchSignFormButton(ThemeData theme, bool isSignIn) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Text(isSignIn
          ? LabelText.defaultSignupButton
          : LabelText.defaultLoginButton),
      disabledTextColor: theme.primaryColor,
      onPressed: _switchAuthMode,
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      textColor: theme.primaryColor,
    );
  }

  Widget _socialSignContainer(ThemeData theme) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          MaterialButton(
            height: 40.0,
            minWidth: 120.0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                width: 1.0,
                color: theme.primaryColor,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Text(
              LabelText.socialGoogleButton.toUpperCase(),
              textAlign: TextAlign.left,
              style: TextStyle(
                color: theme.primaryColor,
              ),
            ),
            onPressed: _onSwitchRecoveryPassword,
            splashColor: Colors.redAccent,
          ),
          SizedBox(
            width: 20,
          ),
          MaterialButton(
            height: 40.0,
            minWidth: 120.0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                width: 1.0,
                color: theme.primaryColor,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Text(
              LabelText.socialFacebookButton.toUpperCase(),
              textAlign: TextAlign.left,
              style: TextStyle(
                color: theme.primaryColor,
              ),
            ),
            onPressed: _onSwitchRecoveryPassword,
            splashColor: Colors.redAccent,
          )
        ],
      ),
    );
  }
}
