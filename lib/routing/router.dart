import 'package:flutter/material.dart';

import 'routes.dart';

import 'package:codfee/screens/sign.dart';
import 'package:codfee/screens/home.dart';
import 'package:codfee/screens/landing.dart';
import 'package:codfee/screens/profile.dart';

Route<dynamic> genRoute(RouteSettings settings) {
  switch (settings.name) {
    case Routes.Landing:
      return MaterialPageRoute(builder: (context) => LandingScreen());
    case Routes.Sign:
      return MaterialPageRoute(builder: (context) => SignScreen());
    case Routes.Home:
      return MaterialPageRoute(builder: (context) => HomeScreen());
    case Routes.Profile:
      return MaterialPageRoute(builder: (context) => ProfileScreen());
    default:
      return MaterialPageRoute(builder: (context) => LandingScreen());
  }
}
