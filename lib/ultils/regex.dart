class Regex {
  static final RegExp simpleEmail = RegExp(r'^[^\s@]+@[^\s@]+\.[^\s@]+$');
  static final RegExp emailRegex = RegExp(
      r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
}
