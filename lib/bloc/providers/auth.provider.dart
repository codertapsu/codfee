import 'package:codfee/models/login-data.model.dart';
import 'package:flutter/material.dart';

enum AuthMode { Signup, SignIn }

/// The result is an error message, callback successes if message is null
typedef AuthCallback = Future<String> Function(LoginData);

/// The result is an error message, callback successes if message is null
typedef RecoverCallback = Future<String> Function(String);

class AuthProvider with ChangeNotifier {
  AuthProvider({
    this.onLogin,
    this.onSignup,
    this.onRecoverPassword,
    AuthProvider previous,
  }) {
    if (previous != null) {
      _mode = previous.mode;
    }
  }

  AuthProvider.empty()
      : this(
          onLogin: null,
          onSignup: null,
          onRecoverPassword: null,
          previous: null,
        );

  final AuthCallback onLogin;
  final AuthCallback onSignup;
  final RecoverCallback onRecoverPassword;

  AuthMode _mode = AuthMode.SignIn;

  AuthMode get mode => _mode;
  set mode(AuthMode value) {
    _mode = value;
    notifyListeners();
  }

  bool get isLogin => _mode == AuthMode.SignIn;
  bool get isSignup => _mode == AuthMode.Signup;
  bool isRecover = false;

  AuthMode opposite() {
    return _mode == AuthMode.SignIn ? AuthMode.Signup : AuthMode.SignIn;
  }

  AuthMode switchAuth() {
    if (mode == AuthMode.SignIn) {
      mode = AuthMode.Signup;
    } else if (mode == AuthMode.Signup) {
      mode = AuthMode.SignIn;
    }
    return mode;
  }
}
