import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

import 'shared/observers/transition-route.observer.dart';

import 'routing/routes.dart';
import 'routing/router.dart' as router;

import 'themes/theme-data.dart';

import 'abstracts/stop-able.service.dart';

import 'services/locator.service.dart';
import 'services/background.service.dart';
import 'services/navigation.service.dart';

Future<void> main() async {
  setupLocator();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return LifeCycleManager(
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: true,
        theme: primaryTheme(),
        navigatorKey: locator<NavigationService>().navigationKey,
        navigatorObservers: [TransitionRouteObserver()],
        onGenerateRoute: router.genRoute,
        initialRoute: Routes.Sign,
      ),
    );
  }
}

class LifeCycleManager extends StatefulWidget {
  final Widget child;
  LifeCycleManager({Key key, this.child}) : super(key: key);

  @override
  _LifeCycleManagerState createState() => _LifeCycleManagerState();
}

class _LifeCycleManagerState extends State<LifeCycleManager>
    with WidgetsBindingObserver {
  List<StopAbleService> _servicesManage = [
    locator<BackgroundService>(),
  ];

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  // Manage state below
  @override
  void didChangeAppLifecycleState(AppLifecycleState appLifecycleState) {
    super.didChangeAppLifecycleState(appLifecycleState);
    _servicesManage.forEach((service) {
      if (appLifecycleState == AppLifecycleState.resumed) {
        service.start();
      } else {
        service.stop();
      }
    });
  }
}
