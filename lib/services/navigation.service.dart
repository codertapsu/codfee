import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigationKey =
      new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName, {dynamic args}) =>
      navigationKey.currentState.pushNamed(routeName, arguments: args);

  bool goBack() => navigationKey.currentState.pop();

  Future<dynamic> signOut() => navigationKey.currentState
      .pushNamedAndRemoveUntil('', (Route<dynamic> route) => false);
}
