import 'package:codfee/abstracts/stop-able.service.dart';

class BackgroundService extends StopAbleService {
  @override
  void start() {
    super.start();
    // do sthing here
    print('Bg tasks start');
  }

  @override
  void stop() {
    super.stop();
    // do sthing here
    print('Bg tasks Stop');
  }
}
