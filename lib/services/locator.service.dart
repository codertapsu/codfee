import 'package:get_it/get_it.dart';

import 'background.service.dart';
import 'navigation.service.dart';

GetIt locator = GetIt.I;

void setupLocator() {
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
  locator.registerLazySingleton<BackgroundService>(() => BackgroundService());
}